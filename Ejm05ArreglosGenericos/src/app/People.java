package app;

import java.lang.reflect.Array;

public class People<T extends Persona> {

	private T[] data = null;
	private Class<T> type = null;
	private int counter = 0;

	public People(Class<T> type) {
		// TODO Auto-generated constructor stub
		data = (T[]) Array.newInstance(type, 10);
		this.type = type;
	}

	public People(Class<T> type, int size) {
		data = (T[]) Array.newInstance(type, size);
		this.type = type;
	}

	public boolean add(T value) {
		if (counter < data.length) {
			data[counter++] = value;
			return true;
		} else
			return false;
	}

	public void printer() {
		for (int i = 0; i < counter; i++) {
			System.out.println(data[i].toString());

			if (data[i] instanceof Empleado) {
				Empleado tmp = (Empleado) data[i];
				System.out.println(tmp.getSalario());
			}
			if (data[i] instanceof alumno) {
				alumno tmp = (alumno) data[i];
				System.out.println(tmp.getMatricula());
			}
		}
	}

}
