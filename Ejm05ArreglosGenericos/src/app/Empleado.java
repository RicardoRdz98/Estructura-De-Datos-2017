package app;

public class Empleado extends Persona {
	private int id;
	private String puesto;
	private double salario;
	
	public Empleado(){
		super();
		this.id = 0;
		this.puesto = "Entrenamiento";
		this.salario = 76d;
	}//fin constructor
	public Empleado(String nombre,int edad,boolean genero){
		super(nombre,edad,genero);
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPuesto() {
		return puesto;
	}
	public void setPuesto(String puesto) {
		this.puesto = puesto;
	}
	public double getSalario() {
		return salario;
	}
	public void setSalario(double salario) {
		this.salario = salario;
	}
	
}
