package linkedList;

import java.util.ArrayList;
import node.node;

public class LinkedList<T> {
	private node<T> sentinel=null;
	
	public LinkedList(){
		sentinel = new node<T>();
		sentinel.setIndex(-1);
		
	}
	public LinkedList(T value) {
		this();
		node<T> tmp = new node<T>(value);
		tmp.setIndex(0);
		sentinel.setNext(tmp);
		
	}
	
/*INICIO AGREGAR*/
	public void addstart(T value) { //AGREGAR ANTES
		node<T> tmp  = sentinel.getNext();
		node<T> _new = new node<T>(value);
		
		_new.setNext(tmp);
		
		sentinel.setNext(_new);
		
	}
	public void addEnd(T value) { //AGREGAR DESPUES
		node<T>tmp =sentinel;
		
		while (tmp.getNext()!=null) 
			tmp = tmp.getNext();
		tmp.setNext(new node<T>(value));
		
	}
/*FIN AGREGAR*/

/*INICIO DE ADDLAST*/
	public boolean addLast(T value, T newvalue){ // AddLast Recursivo
		node<T> finder = Buscar(value);
		if(finder != null)
			return addLast(new node<T>(newvalue), finder); // Mandamos a buscar el nodo
		else
			return false;
	}
	public boolean addLast(T value, node<T> nodo){ // AddLast Recursivo
		node<T> finder = Buscar(value);
		if(finder!= null){
			return addLast(nodo, finder);
		}else{
			return false;
		}
	}
	private boolean addLast(node<T> nodo, node<T> lista){ // AddLast Recursivo
		nodo.setNext(lista.getNext());
		lista.setNext(nodo);
		return true;
	}
/*FIN ADDLAST*/

/*INICIO ADDBEFORE*/
	public boolean addBefore(T value, T newvalue){ // AddLast Recursivo
		node<T>tmp =sentinel;
		
		while (tmp.getNext()!=null && !tmp.getNext().getValue().equals(value)) // <-- While error <-- NO ENTENDI
			tmp = tmp.getNext();
		
		return (tmp.getNext()!=null)?addLast(new node<T>(newvalue),tmp):false; // IMPLEMENTACION ERRONEA <----
	}
/*FIN ADDBEFORE*/

/*INICIO BUSCAR*/
	public boolean buscar(T value){ // BUSCAR INTERATIVO BOOLEAN
		node<T> tmp = sentinel;
		while(tmp.getNext()!=null){
			if(tmp.getNext().getValue().equals(value)){
				return true;
			}else{
				tmp = tmp.getNext();
			}
		}
		return false;
	}
	public boolean buscarr(T value){
		return buscarr(value,sentinel); //BUSCAR RECURSIVO BOOLEAN
	}
	private boolean buscarr(T value, node<T> lista){ //BUSCADO Y RETORNADO
		if(lista.getNext() == null) 
			return false; // NO LO ENCUENTRA
		if(lista.getNext().getValue().equals(value)){
			return true; // LO ENCUENTRA
		}
			return  buscarr(value, lista.getNext());
	}
	private node<T> Buscar(T value){ // BUSCAR INTERATIVO NDOE // <---- PARA USARLO
		node<T> tmp = sentinel;
		while(tmp.getNext()!=null){
			if(tmp.getNext().getValue().equals(value)){
				return tmp.getNext();
			}else{
				tmp = tmp.getNext();
			}
		}
		return null;
	}
	public node<T> BuscarR(T value){
		return BuscarR(value,sentinel); //BUSCAR RECURSIVO NODE
	}
	private node<T> BuscarR(T value, node<T> lista){ //BUSCADO Y RETORNADO NODE
		if(lista.getNext() == null)
			return null;
		if(lista.getNext().getValue().equals(value))
			return lista.getNext();
		return BuscarR(value, lista.getNext());
		
	}
/*FIN BUSCAR*/
	
/*INICIO INDEXOF*/
	public long indexof(T value){ // BUSCAR INTERATIVO BOOLEAN
		node<T> tmp = sentinel;
		while(tmp.getNext()!=null){
			if(tmp.getNext().getValue().equals(value)){
				return tmp.getIndex()+2;
			}else{
				tmp = tmp.getNext();
			}
		}
		
		return 0;
	}
/*FIN INDEXOF*/
	
/*INICIO LIMPIAR*/
	public void eliminar(){
        sentinel.setNext(null); // Elimina el valor y la referencia a los demas nodos.
        sentinel.setIndex(-1); // Reinicia el contador de tama�o de la lista a 0. <--- CHECARLO
    }
/*FIN LIMPIAR*/
	
/*INICIO CHEQUEO*/
	public void isempty() {
		isempty(sentinel);
	}
	private void isempty(node<T> lista) {
		if(lista.getNext() != null) {
			//return true;
			System.out.println("Contiene elementos");
		}else {
			//return false;
			System.out.println("No hay elementos");
		}
	}
/*FIN CHEQUEO*/
	
/*INICIO REMPLAZAR*/
	public void replace(T value, T newvalue){
        if (buscar(value)) {// Consulta si el valor existe en la lista.
        	node<T> tmp = sentinel; // Crea ua copia de la lista.
            while(tmp.getValue() != value){ // Recorre la lista hasta llegar al nodo de referencia.
                tmp = tmp.getNext();
            }
            tmp.setValue(newvalue); // Actualizamos el valor del nodo
        }
    }
/*FIN REMPLAZAR*/
	
/*INICIO BORRADO*/
	public node<T> remmove(T value){ // REMOVER GUARDANDO EL NODO INTERATIVO
		node<T> tmp = sentinel;
		while(tmp.getNext()!=null){
			if(tmp.getNext().getValue().equals(value)){
				tmp.setNext(tmp.getNext().getNext());
				return tmp.getNext(); // regresa el borrado
			}else{
				tmp = tmp.getNext();
			}
		}
		
		return null;
	}
	public boolean remove(T value){ // REMOVER INTERATIVO
		node<T> tmp = sentinel;
		while(tmp.getNext()!=null){
			if(tmp.getNext().getValue().equals(value)){
				tmp.setNext(tmp.getNext().getNext());
				return true;
			}else{
				tmp = tmp.getNext();
			}
		}
		
		return false;
	}
	public boolean remover(T value){
		return remover(value,sentinel); //Remover recursivo V o F
	}
	private boolean remover(T value, node<T> lista){ //Borrado recursivo V o F
		if(lista.getNext() == null) 
			return false; //No lo encontro
		if(lista.getNext().getValue().equals(value)){ //Lo encontro
			lista.setNext(lista.getNext().getNext());
			return true;
		}
			return  remover(value, lista.getNext()); //Regresa al metodo
	}
	/*FIN DE BORRADO*/
	
/*INICIO REMOVEFIRST*/
	public void removefirst(){
		node<T> tmp = sentinel;
        if (tmp.getNext()!=null){
        	tmp.setNext(tmp.getNext().getNext());
        }
    }
/*FIN REMOVEFIRST*/
	
/*INICIO REMOVELAST*/
	public void removelast(){
        node<T> aux = sentinel;
        if(aux.getNext()!=null) {
            while(aux.getNext().getNext() != null){
                aux=aux.getNext();
            }
            aux.setNext(null); //Marcamos el siguiente del antepenultimo como nulo, eliminando el ultimo
        }
 
    }
/*FIN REMOVELAST*/
	
/*INICIO REMOVEBEFORE*/
	public void removebefore(T value){
		try {
		node<T>tmp =sentinel;
		while (tmp.getNext()!=null) 
		{
			if(tmp.getNext().getNext().getValue().equals(value) && tmp.getNext()==sentinel.getNext())
			{
				sentinel.setNext(tmp.getNext().getNext());
			}
			else if(tmp.getNext()!=sentinel.getNext() && tmp.getNext().getNext().getValue().equals(value)) {
				tmp.setNext(tmp.getNext().getNext());
			}
			
			tmp=tmp.getNext();
				
		}
		}catch (Exception e) {
			System.out.println("Es el primer nodo"); // <-- Deve de aver otra forma
		}
		//return false;
	}
/*FIN REMOVEBEFORE*/

/*INICIO REMOVEAFTER*/
	public void removeafter(T value){
		try{node<T> tmp = sentinel;
		while(tmp.getNext()!=null){
			if(tmp.getNext().getValue().equals(value)){
					tmp = tmp.getNext();	
					tmp.setNext(tmp.getNext().getNext());
				return;
			}else{
				tmp = tmp.getNext();
			}
		}
		}catch (Exception e) {
			System.out.println("Es el ultimo nodo"); // <-- Deve de aver otra forma
		}
	}
/*FIN REMOVEAFTER*/

/*INICIO RE-INDEX*/
	public void reindex() {
			node<T> tmp = sentinel;
			int inde = 0;
			while (tmp.getNext()!=null) {
				tmp = tmp.getNext();
				tmp.setIndex(inde);
				inde++;
			}
	}
/*FIN RE-INDEX*/
	
/*INICIO GETFIRST*/
	public void getfirst(){
		node<T> tmp = sentinel;
        if (tmp.getNext()!=null){
        	tmp = tmp.getNext();
            System.out.println(tmp.getValue());
        }
    }
/*FIN GETFIRST*/
	
/*INICIO GETLAST*/
	public void getlast(){
        node<T> tmp = sentinel;
        if (tmp.getNext()!=null){
        	
            while(tmp.getNext() != null){
                tmp = tmp.getNext();
            }
            System.out.println(tmp.getValue());
        }
    }
/*FIN GETLAST*/
	
/*INICIO SIZE*/
	public int size(){
		node<T> tmp = sentinel;
        int numElem = 0;
 
        while(tmp != null){
            numElem++;
            tmp = tmp.getNext();
        }
        return numElem-1;
 
    }
/*FIN SIZE*/
	
/*INICIO IMPRIMIR*/
	public void list() { // LISTAR ITERADOR
		node<T> tmp = sentinel;
		ArrayList<String> iterator = new ArrayList<String>();
		while (tmp.getNext()!=null) {
			tmp = tmp.getNext();
			iterator.add((String) tmp.getValue());
		}

		for (String value : iterator) {
			System.out.println(value);
		}
	}
	public void pronter() { // IMPRIMIR INTERATIVO
		node<T> tmp = sentinel;
		while (tmp.getNext()!=null) {
			
			System.out.print(tmp.getIndex()+1 + "� ");
			System.out.println(tmp.getValue() + " ");
		}
		
	}
	public void printer() { // IMPRIMIR RECURSIVO
		printer(sentinel);
	}
	private void printer(node<T> tmp) {
		if (tmp.getNext()== null)
			return;
		else{
			System.out.print(tmp.getIndex() + "� ");
			System.out.println(tmp.getNext().getValue());
		    printer(tmp.getNext());
		}
	}
/*FIN IMPRIMIR*/

}