package app;

import queve.Queve;

public class App {

	public static void main(String[] args) {
		
		Queve<String> names = new Queve<String>(String.class, 5);
		
		try {
			
			names.enQueve("Jose");
			names.enQueve("Anna");
			names.enQueve("Ricar2");
			names.enQueve("Aaron");
			names.enQueve("Milton");
			System.out.println(names.deQueve());
			//names.enQueve("Evelyn");
			System.out.println(names.front());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
