package app;

@SuppressWarnings("rawtypes")
public class Sales implements Comparable{
	
	private int dia =0; 
	private int mes =0;
	private float venta =0;
	
	public Sales() {
		// TODO Auto-generated constructor stub
	}
	
	public int getDia() {
		return dia;
	}
	public void setDia(int dia) {
		this.dia = dia;
	}
	public int getMes() {
		return mes;
	}
	public void setMes(int mes) {
		this.mes = mes;
	}
	public double getVenta() {
		return venta;
	}
	public void setVenta(float venta) {
		this.venta = venta;
	}
	
	public Sales(int dia, int mes, float venta) {
		super();
		this.dia = dia;
		this.mes = mes;
		this.venta = venta;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Dia: " + this.dia + " Mes: " + this.mes + " Venta: $" + this.venta;
		//return super.toString();
	}
	public double compare(Sales arg0, Sales arg1) {
		// TODO Auto-generated method stub
		if (arg0.venta == arg1.venta)
			return 0;
		if (arg0.venta >= arg1.venta)
			return -1;
		if (arg0.venta <= arg1.venta)
			return 1;
		return 0;
	}
	
	@Override
	public int compareTo(Object arg0) {
		// TODO Auto-generated method stub
		Sales tmp = (Sales)arg0;
		if (this.venta == tmp.venta)
			return 0;
		if (this.venta >= tmp.venta)
			return -1;
		if (this.venta <= tmp.venta)
			return 1;
		return 0;
	}
	@Override
	public boolean equals(Object arg0) {
		// TODO Auto-generated method stub
		return super.equals(arg0);
	}
	
}