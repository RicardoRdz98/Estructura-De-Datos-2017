package app;

import java.util.Scanner;

public class Menu {

	Scanner entradaEscaner = new Scanner (System.in);
	Sales[][] ven = new Sales[12][]; // DECLARACION DE MESES
	
	@SuppressWarnings({ "unchecked" })
	public void Inicio() {
		int res = 0;
		float venta = 0;
		int mes = 0;
		
		/*INICIO DECLARACION DE D�AS*/
		ven[0] = new Sales[31];
		ven[1] = new Sales[28];
		ven[2] = new Sales[31];
		ven[3] = new Sales[30];
		ven[4] = new Sales[31];
		ven[5] = new Sales[30];
		ven[6] = new Sales[31];
		ven[7] = new Sales[31];
		ven[8] = new Sales[30];
		ven[9] = new Sales[31];
		ven[10] = new Sales[30];
		ven[11] = new Sales[31];
		/*FIN DEDECLARACION DE D�AS*/

		int mont = 1;
		for (int i = 0; i < ven.length; i++) {
			System.out.print("Mes "+(i+1)+"�");
			for (int j = 0; j < ven[i].length; j++) {
				ven[i][j]= new Sales(j+1, mont, (float)(Math.random() * 100000 + 500)); // LLENADO DE LAS VENTAS DE CADA DIA DEL MES
				 System.out.print (" ["+ven[i][j]+"]");
				 System.out.print("\t");
			}
			System.out.println("<--");
			mont=mont+1;
		}
		
		do {
			Sales[] D28 = new Sales[28];
			Sales[] D30 = new Sales[30];
			Sales[] D31 = new Sales[31];
			System.out.println("�Que mes decea introducir?");
	        mes = entradaEscaner.nextInt();
	        if(mes>=1 && mes<=12) {
		        System.out.println("�Venta ha introducir?");
		        venta = entradaEscaner.nextFloat();
	        }
	        
	        switch (mes) {
			case 1: //ENERO
				D31 = gnomeSort(ven[0]);
				System.out.println("Su busqueda es: " + Binaria(venta , D31, 0, D31.length-1));
				System.out.println("");
				break;
			case 2: //FEBRERO
				D28 = gnomeSort(ven[1]);
				System.out.println("Su busqueda es: " + Binaria(venta , D28, 0, D28.length-1));
				System.out.println("");
				break;
			case 3: //MARZO
				D31 = gnomeSort(ven[2]);
				System.out.println("Su busqueda es: " + Binaria(venta , D31, 0, D31.length-1));
				System.out.println("");
				break;
			case 4: //ABRIL
				D30 = gnomeSort(ven[3]);
				System.out.println("Su busqueda es: " + Binaria(venta , D30, 0, D30.length-1));
				System.out.println("");
				break;
			case 5: //MAYO
				D31 = gnomeSort(ven[4]);
				System.out.println("Su busqueda es: " + Binaria(venta , D31, 0, D31.length-1));
				System.out.println("");
				break;
			case 6: //JUNIO
				D30 = gnomeSort(ven[5]);
				System.out.println("Su busqueda es: " + Binaria(venta , D30, 0, D30.length-1));
				System.out.println("");
				break;
			case 7: //JULIO
				D31 = gnomeSort(ven[6]);
				System.out.println("Su busqueda es: " + Binaria(venta , D31, 0, D31.length-1));
				System.out.println("");
				break;
			case 8: //AGOSTO
				D31 = gnomeSort(ven[7]);
				System.out.println("Su busqueda es: " + Binaria(venta , D31, 0, D31.length-1));
				System.out.println("");
				break;
			case 9: //SEPTIEMBRE
				D30 = gnomeSort(ven[8]);
				System.out.println("Su busqueda es: " + Binaria(venta , D30, 0, D30.length-1));
				System.out.println("");
				break;
			case 10: //OCTUBRE
				D31 = gnomeSort(ven[9]);
				System.out.println("Su busqueda es: " + Binaria(venta , D31, 0, D31.length-1));
				System.out.println("");
				break;
			case 11: //NOVIEMBRE
				D30 = gnomeSort(ven[10]);
				System.out.println("Su busqueda es: " + Binaria(venta , D30, 0, D30.length-1));
				System.out.println("");
				break;
			case 12: //DICIEMBRE
				D31 = gnomeSort(ven[11]);
				System.out.println("Su busqueda es: " + Binaria(venta , D31, 0, D31.length-1));
				System.out.println("");
				break;
			default:
				System.out.println("Mes incorrecto");
				break;
			}
	        
	        System.out.println("�Decea buscar en otro mes? si = 1 no = 0");
	        res = entradaEscaner.nextInt();
	        
		}while(res == 1);
		System.out.println("GRACIAS POR SU VISITA");
	}
	
	private static <T extends Comparable<T>> T[] gnomeSort(T[] array) {
	      for ( int index = 1; index < array.length; ) {
	         if (array[index - 1].compareTo(array[index]) <= 0) {
	            ++index;
	         } else {
	            T tempVal = (T) array[index];
	            array[index] = array[index - 1];
	            array[index - 1] =  tempVal;
	            --index;
	            if ( index == 0 ) {
	               index = 1;
	            }
	         }
	      }
	      return array;
 }

	
	private Sales Binaria(float dato, Sales[] arreglo, int inicio, int fin) {
		
			int pos = 0;
		
			if(inicio <= fin) {
				pos = (inicio + fin) / 2;
			}else {
				return null;
			}
			if(arreglo[pos].getVenta()== dato) {

				return arreglo[pos];
			}
			if(arreglo[pos].getVenta() > dato) {
				return Binaria(dato, arreglo, pos+1, fin);
			}else {
				return Binaria(dato , arreglo, inicio, pos-1);
			}
		
	}
	
}