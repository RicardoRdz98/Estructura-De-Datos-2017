package app;

import java.lang.reflect.Array;

public class People 	<T> extends Persona {
	
	private T  [] data = null;
	private Class<T> type = null;
	private int cont = 0;
	
	public People(Class <T> type) {
		data = (T[])Array.newInstance(type, 10);
		this.type = type;
	}
	
	public People(Class <T> type, int size) {
		data = (T[])Array.newInstance(type, size);
		this.type = type;
	}
	
	public boolean add(T value) {
		
		
		if(cont < data.length) {
			data[cont++] = value;
			return true;
		}
		return false;
	}
	
	public void Imprimir() {
		for (int i = 0; i < cont; i++) {
			if(data[i] instanceof Empleado){
				System.out.println("Empleado");
				Empleado tmp = (Empleado) data[i];
				System.out.println(tmp.getSalario());
			}
			
			if(data[i] instanceof Alumno){
				System.out.println("Alumno");
				Alumno tmp = (Alumno) data[i];
				System.out.println(tmp.getMatricula()); 
			}
		}
	}
	
}