package stack;

import java.lang.reflect.Array;
import java.util.Iterator;

public class Stack<T extends Comparable <T> > implements iStack <T>, Iterable <T>{
	
	private T  [] data 		= null;
	@SuppressWarnings("unused")
	private Class<T> type 	= null;
//	private int cont = 0;
	private int top 		= -1;
	private int count 		= 0;

	@SuppressWarnings("unchecked")
	public Stack(Class <T> type) {
		data = (T[])Array.newInstance(type, 10);
		this.type = type;
	}

	@SuppressWarnings("unchecked")
	public Stack(Class <T> type, int size) {
		data = (T[])Array.newInstance(type, size);
		this.type = type;
	}

	/*--------------------------------------------*/

	@Override
	public T pop() throws StackEmptyException {
		
		if (isempty()) throw new StackEmptyException("Pila Vacia");
		T tmp = data[top--];
		count--;
		return tmp;
		
	}

	@Override
	public void push(T value) throws StackFullException {
		
		if (isfull()) throw new StackFullException("Pila Llena");
		data[++top] = value;
		count++;
		
	}

	@Override
	public boolean isempty() {
		
		return (count == 0);
		
	}

	@Override
	public boolean isfull() {
		
		return (count == data.length);
		
	}

	@Override
	public T peek() throws StackEmptyException {
		
		if (isempty()) throw new StackEmptyException("Pila Vacia");
		return data[top];
		
	}

	@Override
	public int size() {
		
		return count;
		
	}

	@Override
	public void clear() {
		
		top = -1;
		count = 0;
		
	}

	@Override
	public T search(T value) throws StackEmptyException {
		
		if (isempty()) throw new StackEmptyException("Pila Vacia");
		
		for (T t : data) {
			if (t.equals(value))
				return t;
		}
		return null;
		
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new Iterator<T>() {
			int _top = top;
			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				return (_top >= 0);
			}

			@Override
			public T next() {
				// TODO Auto-generated method stub
				return data[_top--];
			}
		};
	}

}