package stack;

@SuppressWarnings("serial")
public class StackFullException extends Exception {
	
	public StackFullException() {
		// TODO Auto-generated constructor stub
	}
	
	public StackFullException(String causa){
		super (causa);
	}
	
	public StackFullException(Throwable causa){
		super (causa);
	}
	
}
