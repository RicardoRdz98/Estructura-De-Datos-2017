package app;

public class App {

	public static void main(String[] args) {
		
		Object a, b, swap;
		
		a = 45;
		b = 41;
		
		/*swap = a;
		a = b;
		b = swap;
		
		boolean flag = (((Integer)a).intValue() > 0)?true:false; //Casting
		
		System.out.println("El valor de a es " + a);
		System.out.println("El valor de b es " + b);
		System.out.println("\n" + flag);*/
		
		//int [] mayor = {20, 1, 100, 3, 15};
		Mayor(20,1,100,3,15, 500);
	}

	public static <T extends Comparable<T>> T Mayor(T ...mayor){
		T aux = mayor[0];
		
		for (int i = 0; i < mayor.length; i++) {
			if(mayor[i].compareTo(aux) == 1){
				aux = mayor[i];
			}
		}
		
		System.out.println("Mayor " + aux);
		return aux;
	}
	
	//GENERICOS
	public static <T> void swap(T a, T b){
		T tmp;
		
		tmp = a;
		a = b;
		b = tmp;
		
	}
	
}
