package app;

import queve.Queve;
import queve.QueveEmptyException;

public class App {

	public static void main(String[] args) throws QueveEmptyException {
		
		Queve<String> names = new Queve<String>(5);
		
		try {
			
			names.enQueve("Jose");
			names.enQueve("Anna");
			names.enQueve("Ricar2");
			names.enQueve("Aaron");
			names.enQueve("Milton");
			
			System.out.println("Desencolado: " + names.deQueve());
			names.enQueve("Evelyn");
			System.out.println("En cola: " + names.front());
			System.out.println("Resultado: " + names.search("Evelyn"));
			System.out.println("Desencolado: " + names.deQueve());
			System.out.println("En cola: " + names.front());
			
			System.out.println("");
			
			System.out.println("Tama�o: " + names.size());
			
			System.out.println("");
			
//			names.clear();
			
			for (String cola : names) {
				System.out.println(cola);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}